const express = require("express");
const bodyParser = require("body-parser");
const bcrypt = require("bcrypt");

const app = express();
app.set("view engine", "ejs");

//Création des chemins

app.get('/', function(req, res) {
    res.render('index');
});



app.listen(8877, function() {
    console.log('le serveur écoute sur le port 8877');
});